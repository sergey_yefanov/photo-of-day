//
//  ESPViewController.m
//  PhotoOfDay
//
//  Created by Ефанов Сергей on 29.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ESPMainVC.h"

#import "ESPPhotoVC.h"
#import "NSDate+Utilities.h"
#import "SVProgressHUD.h"
#import "PhotoOfDay-Swift.h"

@class ESPContentManager;

@interface ESPMainVC () <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@property (nonatomic) UIPageViewController *pageViewController;
@property (nonatomic) NSInteger index;
@property (nonatomic) NSInteger nextIndex;

@end

@implementation ESPMainVC

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.index = 0;
	self.nextIndex = self.index;
	
	self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl
															  navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
																			options:nil];
    self.pageViewController.dataSource = self;
	self.pageViewController.delegate = self;
	
    [self.pageViewController setViewControllers:@[[self viewcontrollerAtIndex:self.nextIndex]]
									  direction:UIPageViewControllerNavigationDirectionForward
									   animated:NO
									 completion:nil];

    [self.view addSubview:_pageViewController.view];
    
}

#pragma mark - PageViewControllerDelegate

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
	  viewControllerBeforeViewController:(UIViewController *)viewController
{
	self.nextIndex--;
	return [self viewcontrollerAtIndex:self.nextIndex];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
	   viewControllerAfterViewController:(UIViewController *)viewController
{
	if (self.index >= 0) {
		
		return nil;
		
	}
	
	self.nextIndex++;
	return [self viewcontrollerAtIndex:self.nextIndex];
	
}

- (void)pageViewController:(UIPageViewController *)pageViewController
		didFinishAnimating:(BOOL)finished
   previousViewControllers:(NSArray *)previousViewControllers
	   transitionCompleted:(BOOL)completed
{
	
	if (completed) {
		
		self.index = self.nextIndex;
		
	} else {
		
		self.nextIndex = self.index;
		
	}
	
}

#pragma mark - View Controller

- (UIViewController *)viewcontrollerAtIndex:(NSInteger)index
{
	
	ESPPhotoVC *photoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ESPPhotoVC"];
	photoVC.date = [[[NSDate date] dateAtStartOfDay] dateByAddingDays:index];
	
    [SVProgressHUD performSelector:@selector(show) withObject:nil afterDelay:0.01];
	
	[[ESPContentManager sharedInstance] imageWithDate:photoVC.date
                                         block:^(UIImage * image) {
        
                                             [SVProgressHUD performSelector:@selector(dismiss) withObject:nil afterDelay:0.01];
                                             
                                             if (image) {
                                                 
                                                photoVC.image = image;
                                                 
                                             } else {
                                                 
                                                 [[[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"Connection error", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                                                 
                                             }
                                            
                                        }];
	
	return photoVC;
	
}

@end
