//
//  main.m
//  PhotoOfDay
//
//  Created by Ефанов Сергей on 29.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESPAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([ESPAppDelegate class]));
	}
}
