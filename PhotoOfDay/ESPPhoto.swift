//
//  Entity.swift
//  PhotoOfDay
//
//  Created by Sergey Yefanov on 12.10.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

import Foundation
import CoreData

@objc(ESPPhoto)

class ESPPhoto: NSManagedObject {

    @NSManaged var file: NSData
    @NSManaged var date: NSDate

}
