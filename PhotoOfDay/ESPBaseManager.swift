//
//  ESPBaseManager.swift
//  PhotoOfDay
//
//  Created by Ефанов Сергей on 25.09.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

import Foundation
import UiKit

@objc class ESPBaseManager
{
	class var sharedInstance : ESPBaseManager
	{
		struct Singleton
		{
			static let instance = ESPBaseManager ()
		}
		
		return Singleton.instance
	}
	
	func imageWithDate(date : NSDate) -> UIImage!
	{
        let appDelegate : ESPAppDelegate = UIApplication.sharedApplication().delegate as ESPAppDelegate
        let context: NSManagedObjectContext = appDelegate.managedObjectContext
        
        let request = NSFetchRequest(entityName: "ESPPhoto")
        request.predicate = NSPredicate(format: "date = %@", date)
        
        let array : NSArray = context.executeFetchRequest(request, error: nil)!
        
        if array.count == 0 {
            
            return nil
            
        }
        
        var results : ESPPhoto = array.firstObject as ESPPhoto
        
		return UIImage(data: results.file)
	}
    
    func setImage(image : UIImage, date : NSDate)
    {
        let appDelegate : ESPAppDelegate = UIApplication.sharedApplication().delegate as ESPAppDelegate

        let context : NSManagedObjectContext = appDelegate.managedObjectContext;
        let coordinator : NSPersistentStoreCoordinator = appDelegate.persistentStoreCoordinator;

        let object : ESPPhoto = NSEntityDescription.insertNewObjectForEntityForName("ESPPhoto", inManagedObjectContext: context) as ESPPhoto
        object.file = UIImagePNGRepresentation(image)
        object.date = date
        
        context.save(nil)
    }
}
