//
//  ESPContentManager.swift
//  PhotoOfDay
//
//  Created by Ефанов Сергей on 25.09.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

import Foundation
import UiKit

@objc public class ESPContentManager : NSObject {
	
	public class var sharedInstance : ESPContentManager {
		
	struct Singleton {
		
		static let instance = ESPContentManager()
		
		}
		
		return Singleton.instance
	}
    
    public func imageWithDate(date : NSDate, block : (UIImage? -> Void)) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
        
            if let image = ESPBaseManager.sharedInstance.imageWithDate(date) {
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    block(image)
                    
                })
                
            } else {
                
                ESPLoadManager.sharedInstance.imageWithDate(date, successBlock: { (image : UIImage?) in
                    
                    if (image != nil) {
                        
                        ESPBaseManager.sharedInstance.setImage(image!, date: date)
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        block(image)
                        
                    })
                    
                    
                    }, falureBlock: { () in
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            
                            block(nil)
                            
                        })
                
                } )
                
            }
            
        })
    }

}