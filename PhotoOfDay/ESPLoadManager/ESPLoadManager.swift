//
//  ESPLoadManager.swift
//  PhotoOfDay
//
//  Created by Ефанов Сергей on 25.09.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

import Foundation
import UiKit

@objc public class ESPLoadManager : NSObject {
	
	public class var sharedInstance : ESPLoadManager {
		struct Singleton {
			static let instance = ESPLoadManager()
		}
		return Singleton.instance
	}
	
	public func imageWithDate(date : NSDate, successBlock : (UIImage? -> Void), falureBlock : (Void -> Void)) {
		var flagSuccess = false
		
		if let image = self.cache.objectForKey(date) as? UIImage {
			successBlock(image)
			return
		}

        var image : UIImage?
                
        image = self.imageWithDate(date)
        
        if image != nil {
            self.cache.setObject (image!, forKey: date)
            successBlock(image)
        } else {
            falureBlock();
        }

	}
	
	private func strUrlWithDate(date : NSDate) -> NSString? {
		let dateFormatter : NSDateFormatter = NSDateFormatter();
		
		dateFormatter.dateFormat = "dd"
		let day = dateFormatter.stringFromDate(date)
		
		dateFormatter.dateFormat = "MM"
		let month = dateFormatter.stringFromDate(date)
		
		dateFormatter.dateFormat = "YYYY"
		let year = dateFormatter.stringFromDate(date)
		
		let urlString = NSString(format: "http://api-fotki.yandex.ru/api/podhistory/poddate;%@-%@-%@T12:00:00Z/?limit=1", year, month, day)
		
		let url = NSURL(string: urlString)
        let data : NSData? = NSData(contentsOfURL: url!)
        
        if data == nil {
            
            return nil
            
        }
        
		let ret = NSString(data: data!, encoding: NSUTF8StringEncoding)
		
		NSLog("ret = %@", ret!)
		
        let xmlDict = NSDictionary(XMLString: ret)
        
        let entry: NSDictionary? = xmlDict["entry"] as? NSDictionary
        
        if entry == nil
        {
            return nil
        }
        
        let arrayImg = entry!["f:img"] as NSArray
		
        var strDate = (arrayImg.lastObject as NSDictionary)["_href"] as String
        
        //фото высокого качества
//        var strDate = ((xmlDict["entry"] as NSDictionary)["content"] as NSDictionary)["_src"] as String

		
		return strDate;
	}
	
	private func imageWithDate(date : NSDate) -> UIImage?
    {
        
		let url = self.strUrlWithDate(date)
        
        if url == nil {
            
            return nil
            
        }
        
		let imageUrl = NSURL(string: url!)
		let imageData = NSData(contentsOfURL: imageUrl!)
		return UIImage(data: imageData!)
	}
	
	private var cache : NSCache {
		
		struct Singleton {
			
			static let cache = NSCache()
			
			}
			
			return Singleton.cache
	}
	
}